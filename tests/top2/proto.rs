extern "C" {
    pub fn complex(x: *const i8, ...) -> *mut c_int;
    pub fn another();
}
#[no_mangle]
pub unsafe extern "C" fn use_() -> c_int {
    complex(ptr::null_mut());
    use_();
    another();
}
extern "C" {
    pub fn before_f(x: *const i8) -> *mut c_int;
}
unsafe fn f() { }
unsafe fn use2() { before_f(ptr::null_mut()); }