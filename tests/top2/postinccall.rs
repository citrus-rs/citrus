extern "C" {
    pub fn foo() -> c_int;
}
unsafe fn test() {
    let mut ar = [];
    let mut t = { let mut c_; let mut _t = ar[c_ = foo()]; ar[c_] += 1; _t };
    foo();
}