unsafe fn t() {
    (2f32 + 2f32).sqrt();
    (2f32 + 2f32).sqrt();
    3.5f32.sqrt().abs();
    (2 + 2).abs();
    3.3f32.powi(2);
    3.3f32.powf(2.2f32);
    let mut f: f32;
    f.sqrt();
}