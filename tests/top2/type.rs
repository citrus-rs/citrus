pub struct Foo {
    pub x: c_int,
    pub y: c_uint,
}
pub struct Bar {
    pub s: i16,
    pub z: u16,
}
pub struct FooBar {
    pub f: f32,
}
pub struct Baz {
    pub c: i8,
    pub d: i8,
    pub v: *mut c_void,
}
pub struct Quz {
    pub a: [c_int; 1],
}
pub enum E { A, B = 2, C, }
pub enum F { X = 1, Y, Z = 100, }
pub union Quux {
    pub a: *mut c_int,
    pub s: *mut *mut Foo,
    pub foo: F,
    pub bar: E,
    pub o: [FooBar; 3],
    pub t: Bar,
}