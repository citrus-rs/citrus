struct Foo {
    int x;
    unsigned int y;
};
typedef struct {
    short s;
    unsigned short z;
} Bar;
typedef struct FooBar {
    float f;
} FooBar;
struct Baz {
    char c;
    signed char d;
    void *v;
};
struct Quz {
    int a[1];
};
enum E { A,B=2,C, };
typedef enum { X=1,Y,Z=100 } F;
union Quux {
    int *a;
    struct Foo **s;
    F foo;
    enum E bar;
    struct FooBar o[3];
    Bar t;
};
