struct Foo {
    // before A comment
    int a;
    long b; // B line comment
    unsigned long c; /* c line comment */
    /* before D comment */
    long long d;
    unsigned long long e;
    float f;
    /* before G */ double g;
    // After G
};
