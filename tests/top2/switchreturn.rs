unsafe fn test(x: c_int) -> c_int {
    return match x { 1 | 2 => 3, 4 => 5, _ => 6, };
}