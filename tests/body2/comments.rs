unsafe fn test_comments() {
    // line-comment on start
    /** block-doc l1
     * l2 */
    let mut x = 1; // line-comment on end
    /* block-end */
}