unsafe fn test_for_complex() {
    let mut s = 3;
    let mut z = 10;
    for mut i in s..10 { boop(i) }
    for mut i in s..10 { boop(i) }
    for mut i in s..10 { boop(i) }
    for mut i in (0..(s + 1)).rev() { boop(i) }
    for mut i in (1..(s + 1)).rev() { boop(i) }
    for mut i in (1..(s + 1)).rev() { boop(i) }
    for mut i in s..z { boop(i) }
    for mut i in s..z { boop(i) }
    for mut i in (z..(s + 1)).rev() { boop(i) }
    for mut i in ((z + 1)..(s + 1)).rev() { boop(i) }
    for mut i in ((z + 1)..(s + 1)).rev() { boop(i) }
    for mut i in 0..z { boop(i) }
    for mut i in 0..z { boop(i) }
    for mut i in (s..11).rev() { boop(i) }
    for mut i in ((s + 1)..11).rev() { boop(i) }
    for mut i in ((s + 1)..11).rev() { boop(i) }
    let mut q = 1;
    q = 200;
    for mut q in (s..(q + 1)).rev() { boop(q) }
    let mut j;
    let mut k;
    j = 0;
    k = 1;
    while j < 10 { boop(j); boop(k); { k += 1; j += 1 } }
    while k != 0 { boop(k); k = k == 0 };
}