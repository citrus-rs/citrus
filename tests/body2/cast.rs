unsafe fn test_cast() {
    [1, 2, 3];
    1i64;
    1f32;
    1;
    let mut redundant = 1 as c_int;
    1 as c_int;
    let mut intptr = &mut (1 as c_int);
    &mut (1 as c_int);
}