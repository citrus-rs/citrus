for(;;) { if (1) break; }

for(int x=1;;) { continue; }

for(int i=0; i < 10; i++) {}
for(int i=0; (i < 10); (i++)) {}

for(int i=10; i >= 0; i--) {}
for(int i=10; i > 0; i--) {}
for(int i=10; i > 0; --i) {}

