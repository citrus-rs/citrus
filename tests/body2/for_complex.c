int s=3, z = 10;

for(int i=s; i < 10; i++) boop(i);
for(int i=s; !(i >= 10); i++) boop(i);
for(int i=s; !!(i < 10); (i++)) boop(i);

for(int i=s; i >= 0; i--) boop(i);
for(int i=s; i > 0; i--) boop(i);
for(int i=s; i > 0; --i) boop(i);

for(int i=s; i < z; i++) boop(i);
for(int i=s; (i < z); (i++)) boop(i);

for(int i=s; i >= z; i--) boop(i);
for(int i=s; i > z; i--) boop(i);
for(int i=s; i > z; --i) boop(i);

for(int i=0; i < z; i++) boop(i);
for(int i=0; (i < z); (i++)) boop(i);

for(int i=10; i >= s; i--) boop(i);
for(int i=10; i > s; i--) boop(i);
for(int i=10; i > s; --i) boop(i);

unsigned long q = 1; q = 200;
for(; q >= s; q--) boop(q);

int j, k;
for(j=0,k=1; j < 10; k++, j++) {boop(j);boop(k);}

for(; k; k=!k) boop(k);

