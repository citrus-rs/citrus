unsafe fn test_blocks() {
    { let mut x = 1; }
    { let mut x = 'a'; }
    { let mut x = "b"; };
}