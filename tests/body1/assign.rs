unsafe fn test_assign() {
    let mut x = 1;
    x *= 2;
    x /= 2;
    x %= 7;
    x += 1;
    x -= 1;
    x <<= 1;
    x >>= 1;
    x &= 7;
    x ^= 2;
    x |= 1;
    x = 2;
}