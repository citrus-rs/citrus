unsafe fn test_arr() {
    let mut a = [];
    a[1] = a[2];
    a[a[1]] = a[2 + 2];
    a[{ 1; 2; { 3; 4; 5 } }] = { 1; { 21; 22 } 3; 4; 5; 6 };
    let mut charr = [];
    charr[1] = ptr::null_mut();
    let mut ptr = &mut charr[2];
    ptr[1] = 0i8;
}