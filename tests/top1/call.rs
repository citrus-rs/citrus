unsafe fn test2(mut a: c_int, mut b: c_long, mut c: f32) -> c_int {
    return 1;
    test2(test2(1, 2, 3f32), test2(4, -5, 6.7f32), 8f32);
}
#[no_mangle]
pub unsafe extern "C" fn test() { test2(1, 2, 3f32); (test)(); return; }