void other(int *plain, int sized[5]);

void fn(int *as_arr, int sized_arr[5], int sized_arr_off[5], int *offs, int *nullable, int *ref) {
    if (nullable) {
        as_arr[1] = *ref;
        sized_arr_off += 1;
        offs + 1;
        other(offs, nullable);
    }
}
