typedef struct Foo {} Foo;
typedef struct Bar {} *BarPtr;

void f1(Foo arg1, const Foo arg2, const Foo *arg3, const Foo arg4[]) {}
void f2(BarPtr arg1, const BarPtr arg2, const BarPtr *arg3, const BarPtr arg4[]) {}
void f3(struct Foo arg1, const struct Foo arg2, const struct Foo *arg3, const struct Foo arg4) {}

static void f1s(Foo arg1, const Foo arg2, const Foo *arg3, const Foo arg4[]) {}
static void f2s(BarPtr arg1, const BarPtr arg2, const BarPtr *arg3, const BarPtr arg4[]) {}
static void f3s(struct Foo arg1, const struct Foo arg2, const struct Foo *arg3, const struct Foo arg4[]) {}
