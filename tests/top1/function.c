void vararg(int a1, ...) {}

void c1(const int a1, const int *a2, int arr3[]) {}

void c2(const int a1, const int *const a2, const int arr3[]) {}
static void c2_static(const int a1, const int *const a2, const int arr3[]) {}

void c3(const int *a2, int *const a3) {}
