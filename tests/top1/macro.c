#include <limits.h>

#undef BAR
#define FOO (1+1)
#define FN(a) (a+1)

#define TOPLEVEL int x = 1
TOPLEVEL;

static void test(void) {
    int x = FOO + CHAR_BIT;
    int y = FOO;
    int z = FN(2) + FN(FOO);
}



