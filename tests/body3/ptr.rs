unsafe fn test_ptr() {
    let mut ptr: *mut *mut c_int;
    ptr.is_null();
    (*ptr).is_null();
    (**ptr) == 0;
    ptr = ptr.add(1);
    ptr = ptr.add(1);
    ptr = ptr.sub(1);
    ptr = ptr.sub(1);
    *{ let mut _t = ptr; ptr = ptr.add(1); _t };
    (*ptr) = (*ptr).add(1);
    (**ptr) += 1;
    ptr = ptr.add(2);
    ptr = ptr.sub(2);
    *ptr = (*ptr).add(2);
    **ptr += 2;
    ptr[1];
    ptr;
    let mut arr = [];
    (*arr) += 1;
    *arr += 2;
}