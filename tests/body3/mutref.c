int mutable = 1;
int *mutable_ref = &mutable;

const int immutable = 1;
const int *const immutable_ref = &immutable;
