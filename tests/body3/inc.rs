unsafe fn test_inc() {
    let mut predec = 1;
    predec -= 1;
    let mut postdec = { predec -= 1; predec };
    postdec -= 1;
    let mut a = 1;
    let mut b =
        { a += 1; a } + { let mut _t = a; a += 1; _t } + { a -= 1; a } -
            { a -= 1; a };
    let mut arr = [];
    arr[{ let mut _t = a; a += 1; _t }] =
        {
            let mut _t = arr[{ let mut _t = a; a += 1; _t }];
            arr[{ let mut _t = a; a; _t }] += 1;
            _t
        };
}