int **ptr;

!ptr;
!(*ptr);
!(**ptr);

ptr++;
++ptr;
ptr--;
--ptr;
*ptr++;
(*ptr)++;
(**ptr)++;

ptr += 2;
ptr -= 2;
*ptr += 2;
**ptr += 2;

&(*ptr[1]);
&*ptr;

int arr[10] = {};

(*arr)++;
*arr += 2;

