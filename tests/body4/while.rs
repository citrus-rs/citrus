unsafe fn test_while() {
    let mut x = 1;
    while x > 0 { x -= 1; }
    while 1 != 0 { }
    loop  { x += 1; if 0 == 0 { break  }; }
    loop  { x -= 1; if x == 0 { break  }; };
}