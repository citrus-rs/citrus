struct Foo {int foo;};
struct Bar {struct Foo foo;};

struct Foo var = {1};
struct Foo arr[] = {{1}, {2}, {3}};
struct Foo designated = {.foo=1};
struct Foo arrdesignated[] = {{.foo=1}, {.foo=2}, {.foo=3}};

struct Bar nested = {{1}};
struct Bar nestedarr[] = {{{1}}};

struct Baz {int inlined;} complex[] = {{1},{2}};
union Quz {int inlined; long other_field;} complex_union[] = {{1},{2},{.other_field=3}};
