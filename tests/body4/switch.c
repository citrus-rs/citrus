int x=1;
switch(x) {
    case 1: x=2; break;
    case 2:
    case 3: x=3; break;
    case 4:
    case 5:
        x=-x; x++;
        if (1) { x--; break; } else {
            if (2) {break;} else {break;}
        }
        while (1) {/*unreachable*/}
    default:
        x=-1;
        for(;;) break;
}

switch(1) {
    default:
    case 2:
        { x = 1; }
    case 3:
    case 4:
        x = 4;
}
