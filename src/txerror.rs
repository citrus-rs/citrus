use std::fmt;
use std::io;
use std::mem;
use std::error::Error as StdError;
use c3::Error as C3Error;

use syntex_syntax::codemap::*;

#[derive(Debug)]
pub enum Error {
    Str(String, Option<Box<Error>>),
    Spanned(String, Span, Option<Box<Error>>),
    Located(String, String, Option<String>, Option<Box<Error>>),
    Std(Box<dyn StdError + Send + Sync>),
    Io(io::Error),
}

impl Error {
    pub fn context<S: Into<Self>>(self, s: S) -> Self {
        let s = s.into();
        match self {
            Self::Str(msg, None) => Self::Str(msg, Some(Box::new(s))),
            Self::Str(msg, Some(reason)) => Self::Str(msg, Some(Box::new(reason.context(s)))),
            Self::Spanned(msg, span, None) => Self::Spanned(msg, span, Some(Box::new(s))),
            Self::Spanned(msg, span, Some(reason)) => Self::Spanned(msg, span, Some(Box::new(reason.context(s)))),
            Self::Located(..) |
            Self::Std(_) |
            Self::Io(_) => s.context(self),
        }
    }

    pub fn resolve(&mut self, codemap: &CodeMap, full: bool) {
        match *self {
            Self::Str(_, Some(ref mut r)) |
            Self::Located(_, _, _, Some(ref mut r)) => r.resolve(codemap, full),
            _ => {},
        };

        *self = match *self {
            Self::Spanned(ref msg, span, ref mut reason) => {
                let mut r = None; mem::swap(&mut r, reason);
                let loc = codemap.span_to_string(span);
                let snip = if full {codemap.span_to_snippet(span).ok()} else {None};
                if let Some(r) = r.as_mut() { r.resolve(codemap, full && snip.is_none()) }
                Self::Located(msg.clone(), loc, snip, r)
            },
            _ => return,
        };
    }
}

pub type Res<T> = Result<T, Error>;

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Self::Str(ref desc, ref reason) => {
                write!(f, "{desc}")?;
                if let Some(ref reason) = *reason {
                    write!(f, "\n  {reason}")?;
                }
                Ok(())
            },
            Self::Located(ref msg, ref loc, ref snip, ref reason) => {
                write!(f, "{loc}\nerror: {msg}")?;
                if let Some(ref snip) = *snip {
                    write!(f, "\n{snip}")?;
                }
                if let Some(ref reason) = *reason {
                    write!(f, "\n - {reason}")?;
                }
                Ok(())
            },
            Self::Spanned(ref msg, ref loc, ref reason) => {
                write!(f, "{} in @{}", msg, loc.lo.0)?;
                if let Some(ref reason) = *reason {
                    write!(f, "\n - {reason}")?;
                }
                Ok(())
            },
            Self::Std(ref err) => err.fmt(f),
            Self::Io(ref err) => err.fmt(f),
        }
    }
}

impl StdError for Error {

    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match *self {
            Self::Str(_, ref err) |
            Self::Located(_, _, _, ref err) |
            Self::Spanned(_, _, ref err) => Some(err.as_ref()?),
            Self::Std(ref err) => Some(&**err),
            Self::Io(ref err) => Some(err),
        }
    }
}

impl<'a> From<&'a str> for Error {
    fn from(s: &'a str) -> Self {
        Self::Str(s.to_owned(), None)
    }
}

impl From<String> for Error {
    fn from(s: String) -> Self {
        Self::Str(s, None)
    }
}

impl From<String> for Box<Error> {
    fn from(s: String) -> Self {
        Self::new(Error::Str(s, None))
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self::Io(e)
    }
}

impl<S> From<(S, Span)> for Error where S: Into<String> {
    fn from((s, span): (S, Span)) -> Self {
        Self::Spanned(s.into(), span, None)
    }
}

impl From<C3Error> for Error {
    fn from(s: C3Error) -> Self {
        match s {
            C3Error::Str(a, None) => Self::Str(a, None),
            C3Error::Str(a, Some(b)) => Self::Str(a, Some(Box::new(Self::Std(b)))),
            C3Error::Std(a) => Self::Std(a),
            C3Error::Io(a) => Self::Io(a),
        }
    }
}
