use c3::expr::*;
use c3::expr::Kind as K;
use c3::expr::UnOpKind as UK;
use super::{Filter, Change};

pub struct WhileMod;
impl Filter for WhileMod {
    /// `do while(c) x;` => `loop {x; if !c break;}`
    fn while_modify(&self, f: &mut While, _: ()) -> Change {
        if f.is_do {
            let mut block = match f.body.kind {
                K::Block(ref block) => {
                    block.clone()
                },
                ref other => {
                    Block {
                        items: vec![Expr {
                            loc: f.body.loc,
                            kind: other.clone(),
                        }],
                        returns_value: false,
                    }
                },
            };
            block.items.push(Expr {
                loc: f.cond.loc,
                kind: K::If(If {
                    returns_value: false,
                    alt: None,
                    body: Box::new(Expr {
                        loc: f.cond.loc,
                        kind: K::Break,
                    }),
                    cond: Box::new(Expr {
                        loc: f.cond.loc,
                        kind: K::UnaryOperator(
                            UnaryOperator {
                                kind: UK::Not,
                                arg: f.cond.clone(),
                            },
                        ),
                    }),
                }),
            });
            Change::Modify(K::For(For {
                cond: None,
                init: None,
                inc: None,
                body: Box::new(Expr {
                    loc: f.body.loc,
                    kind: K::Block(block),
                }),
            }))
        } else {
            Change::Keep
        }
    }
}
