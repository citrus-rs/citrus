use std::mem;
use c3::expr::*;
use c3::ty::*;
use c3::expr::BinOpKind as BK;
use c3::ty::TyKind as TK;
use c3::expr::Kind as K;
use super::{Filter, Change};

use crate::exprinfo::*;
use crate::builder::*;

/// Add `{}` to `if () foo;`
pub struct IfBlocks;

impl IfBlocks {
    fn block_wrap(kind: &mut K, loc: Loc) {
        match *kind {
            K::Block(ref mut b) => {
                b.returns_value = false;
            },
            K::If(ref mut b) => {
                b.returns_value = false;
            },
            ref mut k => {
                let mut tmp = K::Break; // doesn't matter which
                mem::swap(&mut tmp, k); // no-clone move
                *k = K::Block(Block {
                    items: vec![Expr {
                        loc,
                        kind: tmp,
                    }],
                    returns_value: false,
                });
            },
        }
    }
}

impl Filter for IfBlocks {
    fn if_modify(&self, i: &mut If, _: ()) -> Change {
        if !i.returns_value {
            let loc = i.body.loc;
            Self::block_wrap(&mut i.body.kind, loc);
            if let Some(ref mut alt) = i.alt {
                let loc = alt.loc;
                Self::block_wrap(&mut alt.kind, loc);
            }
        }
        Change::Keep
    }

    fn while_modify(&self, i: &mut While, _: ()) -> Change {
        let loc = i.body.loc;
        Self::block_wrap(&mut i.body.kind, loc);
        Change::Keep
    }
}

/// `if x` needs to be `if x != 0` in Rust
pub struct CastLiterals;

impl Filter for CastLiterals {
    fn cast(&self, c: &mut Cast, _: ()) -> Change {

        let arg = c.arg.ignoring_parens();
        let arg_ty = c.arg.kind.ty();
        let can_be_removed = !c.explicit || match arg.kind {K::Cast(_) => true, _=>false};
        let is_same_type = arg_ty.map_or(false, |t|t == &c.ty);

        if can_be_removed && is_same_type {
            return Change::Replace(arg.clone());
        }

        // Implicit cast to function type is most likely useless
        match c.ty.kind {
            TK::FunctionProto | TK::FunctionNoProto if !c.explicit => {
                return Change::Replace((*c.arg).clone());
            },
            _ => {},
        };

        // const bool != bool :/
        if !is_same_type && !arg_ty.map_or(false, |t|t.kind == TK::Bool) && c.ty.kind == TK::Bool {
            return Change::Modify(K::BinaryOperator(BinaryOperator {
                kind: BK::Ne,
                left: c.arg.clone(),
                right: Kind::int(0).bexpr(c.arg.loc),
            }));
        }

        match arg.ignoring_parens().kind {
            // FIXME: could handle no-None type here too, if performed the cast/overflow on the value (999 as u8 as u32 -> 231 as u32)
            K::IntegerLiteral(val, None) => {
                match c.ty.kind {
                    // FIXME: should suppor switching ILP64 / LP64 / LLP64
                    TK::Int | TK::UInt | TK::Short | TK::UShort |
                    TK::LongLong | TK::ULongLong | TK::Int128 | TK::UInt128 |
                    TK::SChar | TK::UChar | TK::WChar | TK::Char16 | TK::Char32 => {
                        return Change::Modify(K::IntegerLiteral(val, Some(c.ty.clone())));
                    },
                    TK::Float | TK::Double => {
                        return Change::Modify(K::FloatingLiteral(val as f64, Some(c.ty.clone())));
                    },
                    TK::Typedef(ref name) => {
                        let known_typedef = match name.as_str() {
                            "uint64_t" => true,
                            "int64_t" => true,
                            "uint32_t" => true,
                            "int32_t" => true,
                            "int16_t" => true,
                            "uint16_t" => true,
                            "int8_t" => true,
                            "uint8_t" => true,
                            "size_t" => true,
                            "ssize_t" => true,
                            "ptrdiff_t" => true,
                            "intptr_t" => true,
                            "uintptr_t" => true,
                            _ => false,
                        };
                        if known_typedef {
                            return Change::Modify(K::IntegerLiteral(val, Some(c.ty.clone())));
                        }
                    },
                    _ => {},
                }
            },
            K::FloatingLiteral(val, None) => {
                match c.ty.kind {
                    TK::Float | TK::Double => {
                        return Change::Modify(K::FloatingLiteral(val, Some(c.ty.clone())));
                    },
                    _ => {},
                }
            }
            _ => {},
        };

        // Implicit casts between very different types are required.
        if !c.explicit {
            if let Some(arg_ty) = arg_ty {
                // For now it only handles implicit void* conversion
                let make_auto_type = match (arg_ty.target_type(), c.ty.target_type()) {
                    (Some(a), Some(b)) if a != b => match (&a.kind, &b.kind) {
                        (&TK::Void, _) => {
                            c.explicit = true;
                            None
                        },
                        (_, &TK::Void) => {
                            c.explicit = true;
                            Some(Box::new(b.clone()))
                        },
                        _ => None,
                    },
                    _ => None,
                };
                // `as *mut _` is nicer than `as *mut c_void`
                if let Some(mut target) = make_auto_type {
                    target.kind = TK::Auto;
                    c.ty.kind = TK::Pointer(target);
                }
            }
        }

        Change::Keep
    }
}

/// replace init list with compound literal
/// `Struct x = {1}` => `x = Struct{1}`
pub struct VarCompound;
impl Filter<Option<Ty>> for VarCompound {
    fn child_context(&self, expr: &mut Expr, ty: Option<Ty>) -> Option<Ty> {
        fn inner_type(ty: Option<Ty>) -> Option<Ty> {
            ty.and_then(|ty| match ty.kind {
                TK::ConstantArray(_, ty) |
                TK::VariableArray(_, ty) |
                TK::IncompleteArray(ty) => Some(*ty),
                _ => None,
            })
        }

        match expr.kind {
            K::InitList(_) => inner_type(ty),
            K::CompoundLiteral(ref c) => inner_type(Some(c.ty.clone())),
            K::VarDecl(ref var) if var.init.is_some() => var.ty.clone(),
            K::VarDecl(ref var) if var.init.is_none() => None,
            K::For(_) | K::While(_) | K::FunctionDecl(..) | K::Switch(_) | K::ExternC(_) => None,
            K::DesignatedInit(..) => None,
            _ => ty,
        }
    }

    fn init_list(&self, items: &mut Vec<Expr>, ty: Option<Ty>) -> Change {
        if let Some(ty) = ty {
            Change::Modify(K::CompoundLiteral(CompoundLiteral {
                items: items.clone(),
                ty,
            }))
        } else {
            Change::Keep
        }
    }
}


