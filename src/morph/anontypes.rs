use c3::expr::*;
use c3::expr::Kind as K;
use c3::ty::TyKind as TK;
use super::{Filter, Change};

pub struct AnonTypes {}
impl AnonTypes {
    fn is_anon_name(name: &str) -> bool {
        name.starts_with("anon ")
    }
}

impl Filter for AnonTypes {
    fn ty_decl(&self, ty_decl: &mut TyDecl, _: ()) -> Change {

        // Top-level anonymous struct definition is a redundant copy of a reference
        // from another type/expr. It can be ignored, because a typedef or var_decl will
        // surface it later with a proper name.
        if Self::is_anon_name(&ty_decl.name) {
            return Change::ReplaceExprs(vec![]);
        }

        if let TK::Elaborated(ref name, Some(ref ty)) = ty_decl.ty.kind {

            // Typedef struct {} Foo; => struct Foo {}
            if Self::is_anon_name(name) {
                return Change::Modify(K::TyDecl(TyDecl {
                    name: ty_decl.name.clone(),
                    ty: *ty.clone(),
                }));
            }

            match ty.kind {
                // Delete typedef struct Foo Foo;
                TK::Struct(ref inner_name, _) |
                TK::Union(ref inner_name, _) |
                TK::Enum(ref inner_name, _) if name == inner_name => {
                    return Change::ReplaceExprs(vec![]);
                },
                _ => {},
            }
        };
        Change::Keep
    }

    fn var_decl(&self, var: &mut VarDecl, _: ()) -> Change {
        if let Some(ref mut ty) = var.ty {
            match ty.kind {
                TK::Elaborated(ref mut name, _) if Self::is_anon_name(name) => {
                    *name = var.name.clone();
                },
                _ => {},
            };
        }
        Change::Keep
    }
}
