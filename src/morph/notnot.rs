use c3::expr::*;
use c3::expr::Kind as K;
use c3::expr::UnOpKind as UK;
use c3::expr::BinOpKind as BK;
use super::{Filter, Change};
use crate::builder::*;
use crate::exprinfo::*;

/// `!(x != y)` to `x == y` (cleans up excessive negation from boolean context casts)
/// Also removes &*
pub struct NotNot;
impl Filter for NotNot {
    fn unary(&self, op: &mut UnaryOperator, _: ()) -> Change {
        match op.kind {
            UK::AddrOf(_) => if let K::UnaryOperator(UnaryOperator {
                    kind: UK::Deref,
                    ref arg,
                }) = op.arg.ignoring_wrappers_mut().kind {
                return Change::Replace((**arg).clone());
            },
            UK::Not => {
                let flipped = match op.arg.ignoring_wrappers_mut().kind {
                    K::BinaryOperator(BinaryOperator {
                        ref mut kind,
                        ..
                    }) => {
                        let flip = match *kind {
                            BK::Eq => Some(BK::Ne),
                            BK::Ne => Some(BK::Eq),
                            BK::Lt => Some(BK::Ge),
                            BK::Le => Some(BK::Gt),
                            BK::Ge => Some(BK::Lt),
                            BK::Gt => Some(BK::Le),
                            _ => None,
                        };
                        if let Some(flip) = flip {
                            *kind = flip;
                            true
                        } else {
                            false
                        }
                    },
                    _ => false,
                };
                if flipped {
                    return Change::Replace((*op.arg).clone());
                }
            },
            _ => {},
        }
        Change::Keep
    }
}
