use c3::expr::*;
use c3::expr::Kind as K;
use std::mem;
use super::{Filter, Change};

/// Pair of filters: 1. find switch, 2. remove break; statements from it
/// (because match doesn't support them)
pub struct FilterSwitchBreak;
impl Filter for FilterSwitchBreak {

    /// 2. Flatten `Case(Case(Case(…)))` into `Case()Case()Case()…`
    ///
    /// There's no point keeping case.items, since clang is nesting cases
    /// inconsistently
    fn case(&self, case: &mut Case, loc: &Loc, _ctx: ()) -> Change {
        if !case.items.is_empty() {
            let mut items = vec![];
            mem::swap(&mut case.items, &mut items);
            items.insert(0, Expr {loc: *loc, kind: K::Case(case.clone())});
            Change::ReplaceExprs(items)
        } else {
            Change::Keep
        }
    }

    fn switch(&self, switch: &mut Switch, loc: &Loc, _ctx: ()) -> Change {
        merge_cases(switch);
        clone_arm_bodies(switch);
        move_default_last(switch);
        Change::Keep
    }
}

/// C allows default anywhere (it's a goto label), but Rust evaluates in order
fn move_default_last(switch: &mut Switch) {
    let mut i = 0;
    while i < switch.items.len() {
        let is_default = match switch.items[i].kind {
            K::Case(ref c) if c.conds.is_empty() => true,
            _ => false,
        };
        if is_default {
            let def = switch.items.remove(i);
            switch.items.push(def);
            return; // there isn't going to be another
        }
        i += 1;
    }
}

/// Cases are cloned until break, which correctly duplicates code
/// for fall-through.
fn clone_arm_bodies(switch: &mut Switch) {
    let mut arms = vec![];
    for (i, e) in switch.items.iter().enumerate() {
        if let K::Case(ref case) = e.kind {
            let (_, items) = clone_exprs_until_break(&switch.items[i..]);
            arms.push(Expr{
                loc: e.loc,
                kind: K::Case(Case {
                    conds: case.conds.clone(),
                    items,
                }),
            });
        };
    }
    switch.items = arms;
}

/// Coalesce case 1: case 2: as case 1|2:
fn merge_cases(switch: &mut Switch) {
    let mut to_remove = vec![];
    {
        let mut current: Option<&mut Vec<Expr>> = None;
        for (i, e) in switch.items.iter_mut().enumerate() {
            match e.kind {
                K::Case(ref mut case) => if let Some(ref mut current) = current {
                    // merging with default can throw away other cases too
                    if current.is_empty() || case.conds.is_empty() {
                        current.clear();
                    } else {
                        current.append(&mut case.conds);
                    }
                    to_remove.push(i);
                } else {
                    current = Some(&mut case.conds);
                },
                _ => {
                    current = None;
                },
            }
        }
    }
    // Waiting for vec.drain_filter
    for i in to_remove.into_iter().rev() {
        switch.items.remove(i);
    }
}


fn clone_exprs_until_break(exprs: &[Expr]) -> (bool, Vec<Expr>) {
    let mut out = Vec::with_capacity(exprs.len());
    for e in exprs {
        let (saw_break, expr) = clone_until_break(e);
        out.push(expr);
        if saw_break {
            return (true, out);
        }
    }
    (false, out)
}


fn clone_until_break(expr: &Expr) -> (bool, Expr) {
    match expr.kind {
        // breaks in nested switch don't count (FIXME: return does :()
        K::Switch(ref e) => (false, Expr{loc:expr.loc, kind:K::Switch(e.clone())}),
        K::For(ref e) => (false, Expr{loc:expr.loc, kind:K::For(e.clone())}),
        K::ForIn(ref e) => (false, Expr{loc:expr.loc, kind:K::ForIn(e.clone())}),
        K::While(ref e) => (false, Expr{loc:expr.loc, kind:K::While(e.clone())}),


        // case is special, as it should be invisible here (will be reconstructed)
        K::Case(ref e) => {
            let (br, items) = clone_exprs_until_break(&e.items);
            (br, Expr{
                loc:expr.loc,
                kind:K::TransparentGroup(TransparentGroup {
                    items,
                })
            })
        },
        // break also needs to disappear as it doesn't make sense in match
        K::Break => (true, Expr{loc:expr.loc, kind:K::TransparentGroup(TransparentGroup {items:vec![]})}),

        K::TyDecl(ref e) => (false, Expr{loc:expr.loc, kind:K::TyDecl(e.clone())}),
        K::RecordDecl(ref e) => (false, Expr{loc:expr.loc, kind:K::RecordDecl(e.clone())}),
        K::FunctionDecl(ref f, ref e) => (false, Expr{loc:expr.loc, kind:K::FunctionDecl(f.clone(), e.clone())}),
        K::FunctionProtoDecl(ref e) => (false, Expr{loc:expr.loc, kind:K::FunctionProtoDecl(e.clone())}),
        K::VarDecl(ref e) => (false, Expr{loc:expr.loc, kind:K::VarDecl(e.clone())}),
        K::InitList(ref e) => (false, Expr{loc:expr.loc, kind:K::InitList(e.clone())}),
        K::TranslationUnit(ref e) => {
            let (br, items) = clone_exprs_until_break(&e.items);
            (br, Expr{
                loc:expr.loc,
                kind:K::TranslationUnit(TranslationUnit {
                    items,
                    name: e.name.clone(),
                })
            })
        },
        K::ExternC(ref e) => (false, Expr{loc:expr.loc, kind:K::ExternC(e.clone())}),
        K::Block(ref e) => {
            let (br, items) = clone_exprs_until_break(&e.items);
            (br, Expr{loc:expr.loc, kind:K::Block(Block {
                items,
                returns_value: e.returns_value,
            })})
        },
        K::Paren(ref e) => {
            let (br, e) = clone_until_break(e);
            (br, Expr{loc:expr.loc, kind:K::Paren(Box::new(e))})
        },
        K::If(ref e) => {
            let cond = e.cond.clone();
            let (body_br, body) = clone_until_break(&e.body);
            let (alt_br, alt) = if let Some(ref alt) = e.alt {
                let (b, e) = clone_until_break(alt);
                (b, Some(Box::new(e)))
            } else {
                (false, None)
            };
            // If then and else both contain break, then it is a real break.
            // Otherwise the code could proceed further.
            (body_br && alt_br, Expr{loc:expr.loc, kind:K::If(If {
                cond,
                body: Box::new(body),
                alt,
                returns_value: e.returns_value,
            })})
        },
        K::Call(ref e) => (false, Expr{loc:expr.loc, kind:K::Call(e.clone())}),
        K::Return(ref e) => (true, Expr{loc:expr.loc, kind:K::Return(e.clone())}),
        K::Goto(ref e) => (true, Expr{loc:expr.loc, kind:K::Goto(e.clone())}),
        K::Label(ref l, ref e) => {
            let (br, e) = clone_until_break(e);
            (br, Expr{loc:expr.loc, kind:K::Label(l.clone(), Box::new(e))})
        },
        K::Continue => (true, Expr{loc:expr.loc, kind:K::Continue}),
        K::DeclRef(ref e) => (false, Expr{loc:expr.loc, kind:K::DeclRef(e.clone())}),
        K::MacroRef(ref e) => (false, Expr{loc:expr.loc, kind:K::MacroRef(e.clone())}),
        K::BinaryOperator(ref e) => {
            let (left_br, left) = clone_until_break(&e.left);
            let (right_br, right) = clone_until_break(&e.right);
            (left_br && right_br, Expr{loc:expr.loc, kind:K::BinaryOperator(BinaryOperator{
                left: Box::new(left),
                right: Box::new(right),
                kind: e.kind,
            })})
        },
        K::UnaryOperator(ref e) => (false, Expr{loc:expr.loc, kind:K::UnaryOperator(e.clone())}),
        K::SizeOf(ref e) => (false, Expr{loc:expr.loc, kind:K::SizeOf(e.clone())}),
        K::OffsetOf(ref t, ref n) => (false, Expr{loc:expr.loc, kind:K::OffsetOf(t.clone(), n.clone())}),
        K::IntegerLiteral(v, ref t) => (false, Expr{loc:expr.loc, kind:K::IntegerLiteral(v, t.clone())}),
        K::StringLiteral(ref e) => (false, Expr{loc:expr.loc, kind:K::StringLiteral(e.clone())}),
        K::FloatingLiteral(e, ref t) => (false, Expr{loc:expr.loc, kind:K::FloatingLiteral(e, t.clone())}),
        K::CharacterLiteral(e) => (false, Expr{loc:expr.loc, kind:K::CharacterLiteral(e)}),
        K::CompoundLiteral(ref e) => {
            let (br, items) = clone_exprs_until_break(&e.items);
            (br, Expr{loc:expr.loc, kind:K::CompoundLiteral(CompoundLiteral {
                items,
                ty: e.ty.clone(),
            })})
        },
        K::Cast(ref e) => {
            let (br, arg) = clone_until_break(&e.arg);
            (br, Expr{loc:expr.loc, kind:K::Cast(Cast {
                arg: Box::new(arg),
                ty: e.ty.clone(),
                explicit: e.explicit,
            })})
        },
        K::MemberRef(ref e) => {
            let (br, arg) = clone_until_break(&e.arg);
            (br, Expr{loc:expr.loc, kind:K::MemberRef(MemberRef {
                arg: Box::new(arg),
                name: e.name.clone(),
            })})
        },
        K::DesignatedInit(ref s, ref e) => (false, Expr{loc:expr.loc, kind:K::DesignatedInit(s.clone(), e.clone())}),
        K::TransparentGroup(ref e) => {
            let (br, items) = clone_exprs_until_break(&e.items);
            (br, Expr{
                loc:expr.loc,
                kind:K::TransparentGroup(TransparentGroup {
                    items,
                })
            })
        },
        K::Invalid(ref e) => (false, Expr{loc:expr.loc, kind:K::Invalid(e.clone())}),
        K::Throw(..) |
        K::Try(..) |
        K::Namespace(..) |
        K::ClassDecl(..) |
        K::CXXConstructor(..) |
        K::CXXDestructor(..) |
        K::CXXMethod(..) => panic!("Unsupported {expr:?}"),
    }
}
