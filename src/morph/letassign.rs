use c3::expr::*;
use c3::expr::Kind as K;
use c3::expr::BinOpKind as BK;
use super::{Filter, Change};
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;
use std::mem;

/// Move var declaration to the first assignment, if possible
pub struct LetAssign {}
impl LetAssign {
    pub fn filter(&self, expr: &mut Expr) {
        self.visit(expr, false);
    }
}

type DeclRef<'a> = Rc<RefCell<HashMap<String, &'a mut K>>>;

/// Remove from the hashmap variables that are used anywhere (used internally)
struct DiscardIfUsed {}
impl<'a> Filter<DeclRef<'a>> for DiscardIfUsed {
    fn decl_ref(&self, name: &mut String, refs: DeclRef<'a>) -> Change {
        refs.borrow_mut().remove(name);
        Change::Keep
    }
}

impl Filter<bool> for LetAssign {
    fn child_context(&self, e: &mut Expr, in_loop: bool) -> bool {
        match e.kind {
            K::FunctionDecl(..) |
            K::Return(_) => false,
            K::For(_) | K::While(_) => true,
            _ => in_loop,
        }
    }

    fn block(&self, b: &mut Block, in_loop: bool) -> Change {
        let defs: DeclRef<'_> = Rc::new(RefCell::new(HashMap::new()));
        for e in &mut b.items {
            // Destructuring keeps references, so wholesale replacement of `e` in the `match` can't be done,
            // hence awkward returning of (Some, Some)
            let declaration = match e.kind {
                K::VarDecl(ref mut v) if v.init.is_none() => Some(v.name.clone()),
                _ => None,
            };
            let replaced = Self::try_replacing(&mut e.kind, &mut defs.borrow_mut());

            if let Some(decl_name) = declaration {
                defs.borrow_mut().insert(decl_name, &mut e.kind);
            } else if !replaced {
                DiscardIfUsed{}.visit(e, defs.clone());

                // Some replacements in loops are possible,
                // but require better analysis.
                if in_loop {
                    break;
                }
            }
        }
        Change::Keep
    }
}

impl LetAssign {
    fn try_replacing(expr_kind: &mut K, defs: &mut HashMap<String, &mut K>) -> bool {
        let replacement = match *expr_kind {
            K::For(For{init: Some(ref mut init), ..}) => {
                return Self::try_replacing(&mut init.kind, defs);
            },
            K::BinaryOperator(BinaryOperator {
                ref left,
                kind: BK::Assign,
                ref right,
            }) => match left.kind {
                K::DeclRef(ref name) => if let Some(def) = defs.remove(name) {
                    match *def {
                        K::VarDecl(ref mut def) => {
                            def.init = Some(right.clone());
                        }
                        _ => unreachable!(),
                    };
                    let mut tmp = K::TransparentGroup(TransparentGroup{items:vec![]});
                    mem::swap(&mut tmp, def);
                    Some(tmp)
                } else {
                    None
                },
                _ => None,
            },
            _ => None,
        };

        if let Some(swap) = replacement {
            *expr_kind = swap;
            true
        } else {
            false
        }
    }
}
