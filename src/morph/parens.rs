use c3::expr::*;
use c3::expr::Kind as K;
use c3::expr::BinOpKind as BK;
use c3::expr::UnOpKind as UK;
use super::{Filter, Change};
use std::collections::HashMap;
use std::collections::hash_map::Entry::*;

use crate::exprinfo::*;
use crate::builder::*;

/// Move var declaration to the smallest scope the var is used in
pub struct Parens {}

impl Filter for Parens {
    fn call(&self, b: &mut Call, _: ()) -> Change {
        if let (Some(selfpri), Some(argpri)) = (b.priority(), b.callee.priority()) {
            if selfpri < argpri {
                b.callee = K::Paren(b.callee.clone()).bexpr(b.callee.loc);
            }
        }
        Change::Keep
    }

    fn cast(&self, b: &mut Cast, _: ()) -> Change {
        if let (Some(selfpri), Some(argpri)) = (b.priority(), b.arg.priority()) {
            if selfpri < argpri {
                b.arg = K::Paren(b.arg.clone()).bexpr(b.arg.loc);
            }
        }
        Change::Keep
    }

    fn member_ref(&self, b: &mut MemberRef, _: ()) -> Change {
        if let (Some(selfpri), Some(argpri)) = (b.priority(), b.arg.priority()) {
            if selfpri < argpri {
                b.arg = K::Paren(b.arg.clone()).bexpr(b.arg.loc);
            }
        }
        Change::Keep
    }

    fn unary(&self, b: &mut UnaryOperator, _: ()) -> Change {
        if let (Some(selfpri), Some(argpri)) = (b.priority(), b.arg.priority()) {
            if selfpri < argpri {
                b.arg = K::Paren(b.arg.clone()).bexpr(b.arg.loc);
            }
        }
        Change::Keep
    }

    fn binary(&self, b: &mut BinaryOperator, _: ()) -> Change {
        if let (Some(selfpri), Some(argpri)) = (b.priority(), b.left.priority()) {
            if selfpri < argpri {
                b.left = K::Paren(b.left.clone()).bexpr(b.left.loc);
            }
        }
        match b.kind {
            BK::ArrayIndex | BK::PointerIndex => {}, // [] unambiguous syntax
            _ => if let (Some(selfpri), Some(argpri)) = (b.priority(), b.right.priority()) {
                if selfpri < argpri {
                    b.right = K::Paren(b.right.clone()).bexpr(b.right.loc);
                }
            }
        }
        Change::Keep
    }
}

#[test]
fn prio() {
    let loc = Loc::dummy();
    let add = BK::Add.new(K::int(2).bexpr(loc), K::int(2).bexpr(loc)).bexpr(loc);
    let mut mul = BK::Mul.new(add.clone(), add).expr(loc);
    Parens{}.visit(&mut mul, ());
    if let K::BinaryOperator(ref b) = mul.kind {
        match b.left.kind {
            K::Paren(_) => {},
            _ => panic!("expected parens"),
        }
        match b.right.kind {
            K::Paren(_) => {},
            _ => panic!("expected parens"),
        }
    } else { panic!() }
}

#[test]
fn prio2() {
    let loc = Loc::dummy();
    let mut mem = MemberRef {
        arg: UK::Deref.new(K::int(0).bexpr(loc)).bexpr(loc),
        name: "hello".into(),
    }.expr(loc);
    Parens{}.visit(&mut mem, ());
    println!("{mem:#?}");
    if let K::MemberRef(ref b) = mem.kind {
        match b.arg.kind {
            K::Paren(_) => {},
            _ => panic!("expected parens"),
        }
    } else { panic!() }
}
