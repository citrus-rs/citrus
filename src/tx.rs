//! ## Jargon
//!
//! * AST = Abstract Syntax Tree. Nodes representing exprssions and statements in the language, e.g. `2+2` is `BinaryOperator(IntegerLiteral, add, IntegerLiteral)`.
//! * `expr`, `stmt`, `node` = one node in the AST. This file uses two flavours of AST. C3 (C3Expr) from C, and syntex_syntax for Rust's AST.
//! * `ty`/`TK` = A type, like `int` or `struct Foo` (weird spellings, because `type` is a reserved keyword in Rust).
//! * `Kind`/`ExprKind`/`ItemKind`/`K` = enum for the kind of node (e.g. `K::VarDecl` is for `let var = …`, )
//! * `Loc`/`Span` = location in the source code (line number, file offset).
//!
//! See [C3](https://gitlab.com/citrus-rs/c3)'s [`expr`](https://gitlab.com/citrus-rs/c3/blob/master/src/expr.rs) and `ty` modules for input types.
//! [Syntex](https://docs.rs/syntex_syntax/0.59.1/syntex_syntax/) for output AST types.

use std::path::Path as FSPath;
use std::collections::HashMap;
use std::rc::Rc;

use syntex_syntax::ast as s;
use syntex_syntax::ast::*;
use syntex_syntax::abi::*;
use syntex_syntax::ast::ExprKind as E;
use syntex_syntax::ast::LitKind as L;
use syntex_syntax::ast::ItemKind as I;
use syntex_syntax::ast::StmtKind as S;
use syntex_syntax::symbol::*;
use syntex_syntax::codemap::*;
use syntex_syntax::ptr::P;
use syntex_syntax::tokenstream::TokenStream;

use crate::txerror::Res;
use c3::expr;
use c3::expr::Expr as C3Expr;
use c3::expr::Kind as K;
use c3::ty;
use c3::ty::Ty as C3Ty;
use c3::ty::TyKind as TK;
use c3::ty::Storage;
use c3::expr::BinOpKind as BK;
use c3::expr::UnOpKind as UK;
use crate::exprinfo::*;

pub(crate) struct Tx<'a> {
    root: &'a C3Expr,
    filemap: Rc<FileMap>,
}

impl<'a> Tx<'a> {
    pub fn new(root: &'a C3Expr, filemap: Rc<FileMap>) -> Res<Self> {
        Ok(Tx { root, filemap })
    }

    fn span_loc(&self, loc: &expr::Loc) -> Span {
        let lo = self.filemap.start_pos + BytePos(loc.byte_pos);
        Span {
            lo,
            hi: lo + BytePos(loc.byte_len),
            ctxt: SyntaxContext::empty(),
        }
    }

    fn span(&self, ce: &C3Expr) -> Span {
        self.span_loc(&ce.loc)
    }

    fn string_lit(span: Span, st: &str) -> Expr {
        let is_big = st.len() > 10 && st[1..st.len()-2].split('\n').count() > 3;
        Self::new_expr(E::Lit(P(Spanned{
            node: L::Str(Symbol::intern(st), if is_big {
                StrStyle::Raw(if st.contains('"') {1} else {0})
            } else {
                StrStyle::Cooked
            }),
            span
        })), span)
    }

    fn int_lit(&self, val: u64, ty: Option<LitIntType>, span: Span) -> Expr {
        let node = L::Int(val.into(), ty.unwrap_or(LitIntType::Unsuffixed));
        Self::new_expr(E::Lit(P(Spanned { node, span })), span)
    }

    fn float_lit(&self, val: &str, ty: Option<FloatTy>, span: Span) -> Expr {
        let node = L::Float(Symbol::intern(val), ty.unwrap_or(FloatTy::F32));
        Self::new_expr(E::Lit(P(Spanned { node, span })), span)
    }

    fn new_expr(node: E, span: Span) -> Expr {
        Expr {
            id: DUMMY_NODE_ID,
            span,
            node,
            attrs: ThinVec::new(),
        }
    }

    fn make_incomplete_expr_tuple(&self, expr: Vec<P<Expr>>, span: Span) -> Expr {
        Expr {
            id: DUMMY_NODE_ID,
            span,
            attrs: ThinVec::new(),
            node: E::Tup(expr),
        }
    }

    pub fn translate_crate(&self) -> Res<Crate> {
        self.crate_from_expr(self.root)
    }

    fn local_var(&self, span: Span, name: &str, arg: Expr) -> Stmt {
        Self::new_stmt(span, S::Local(P(Local {
            pat: P(self.pat_from_str(name, Mutability::Immutable, span)),
            ty: None,
            init: Some(P(arg)),
            id: DUMMY_NODE_ID,
            span,
            attrs: ThinVec::new(),
        })))
    }

    fn new_stmt(span: Span, node: StmtKind) -> Stmt {
        Stmt {
            id: DUMMY_NODE_ID,
            span,
            node,
        }
    }

    fn new_stmt_expr(span: Span, node: Expr) -> Stmt {
        Self::new_stmt(span, S::Expr(P(node)))
    }

    fn new_method_call(&self, span: Span, name: &str, args: Vec<P<Expr>>) -> Expr {
        Self::new_expr(E::MethodCall(
            self.spanned(span, Ident::from_str(name)),
            vec![],
            args,
        ), span)
    }

    fn new_fn_call(&self, span: Span, name: &str, ty: Option<Ty>, args: Vec<Expr>) -> Res<Expr> {
        let mut path = Self::path_by_name(name, span);
        if let Some(ty) = ty {
            path.segments.last_mut().unwrap().parameters = Some(P(AngleBracketed(
                AngleBracketedParameterData {
                    lifetimes: vec![],
                    types: vec![P(ty)],
                    bindings: vec![],
                },
            )));
        }
        let method = Self::new_expr(E::Path(None, path), span);
        Ok(Self::new_expr(
            E::Call(P(method), args.into_iter().map(P).collect()),
            span,
        ))
    }

    fn initializers(&self, items: &[C3Expr], fields: &[ty::Field], span: Span) -> Res<Vec<Field>> {
        let mut designated: HashMap<&str,&C3Expr> = items.iter()
            .filter_map(|e| match e.kind {
                K::DesignatedInit(ref name, ref arg) => Some((name.as_str(), &**arg)),
                _ => None,
            }).collect();
        let mut other = items.iter()
            .filter(|e| match e.kind {K::DesignatedInit(..)=>false,_=>true});

        fn make_field(expr: Expr, name: &str) -> Field {
            let span = expr.span;
            Field {
                is_shorthand: false,
                span,
                expr: P(expr),
                attrs: ThinVec::new(),
                ident: Spanned {
                    span,
                    node: Ident::from_str(name),
                },
            }
        }

        let mut inits = fields.iter().filter_map(|field| {
            designated.remove(field.name.as_str()).or_else(||other.next()).map(|arg| (arg, field))
        })
        .map(|(arg, field)| {
            Ok(make_field(self.expr(arg)?, &field.name))
        }).collect::<Res<Vec<_>>>()?;

        let leftover = designated.drain().map(|(name,arg)|{
            Ok(make_field(self.expr(arg)?, name))
        })
        .chain(other.enumerate().map(|(i,arg)|{
            Ok(make_field(self.expr(arg)?, &format!("_{i}")))
        }))
        .collect::<Res<Vec<_>>>()?;
        if !leftover.is_empty() {
            eprintln!("Excess initializers ({}), expected {} fields {:?}", leftover.len(), fields.len(), span);
            inits.extend(leftover);
        }
        Ok(inits)
    }

    fn compound_literal(&self, ch: &expr::CompoundLiteral, ty: &C3Ty, span: Span) -> Res<Expr> {
        Ok(match ch.ty.kind {
            TK::Struct(ref name, ref fields) | TK::Union(ref name, ref fields) => {
                Self::new_expr(E::Struct(Self::path_by_name(name, span), self.initializers(&ch.items, fields, span)?, None), span)
            },
            TK::ConstantArray(_, _) | TK::VariableArray(_, _) | TK::DependentSizedArray | TK::IncompleteArray(_) => {
                let exprs: Res<Vec<_>> = ch.items.iter().map(|e|self.expr(e).map(P)).collect();
                Self::new_expr(E::Array(exprs?), span)
            },
            TK::Typedef(ref name) | TK::Elaborated(ref name, None) => {
                eprintln!("warning: initializer of unknown struct {name} {span:?}");
                Self::new_expr(E::Struct(Self::path_by_name(name, span), self.initializers(&ch.items, &[], span)?, None), span)
            },
            TK::Elaborated(ref name, Some(ref ty)) => {
                let fields = match ty.kind {
                    TK::Struct(_, ref fields) | TK::Union(_, ref fields) => &fields[..],
                    ref k => {
                        eprintln!("not struct? {k:?}");
                        &[]
                    },
                };
                Self::new_expr(E::Struct(Self::path_by_name(name, span), self.initializers(&ch.items, fields, span)?, None), span)
            },
            _ => {
                // FIXME: that's an unreliable fallback (will trip on designatedinit)
                if ch.items.len() ==1 {
                    let ty = self.ty_from_ty(&ch.ty, span)
                        .map_err(|err| err.context(format!("in CompoundLiteral {ch:#?}")))?;
                    Self::new_expr(E::Cast(
                        P(self.expr(&ch.items[0])?),
                        P(ty)),
                    span)
                } else {
                    Err((format!("bad kind {:#?}, in {:#?}; of type {:#?}", ch.ty.kind, ch, ch.ty), span))?
                }
            }
        })
    }

    fn expr(&self, ce: &C3Expr) -> Res<Expr> {
        let span = self.span(ce);
        Ok(match ce.kind {
            K::CompoundLiteral(ref ch) => {
                self.compound_literal(ch, &ch.ty, span)?
            },
            K::DeclRef(ref name) => {
                Self::new_expr(E::Path(None, Self::path_by_name(name, span)), span)
            },
            K::MacroRef(ref name) => {
                Self::new_expr(E::Path(None, Self::path_by_name(name, span)), span)
            },
            K::Cast(ref ca) => {
                let arg = self.expr(&ca.arg)?;
                // FIXME: use implicit ones somehow?
                if ca.explicit {
                    let ty = self.ty_from_ty(&ca.ty, span)
                        .map_err(|err| err.context(format!("in cast {ca:#?}")))?;
                    Self::new_expr(E::Cast(
                        P(arg),
                        P(ty)),
                    span)
                } else {
                    arg
                }
            },
            K::Call(ref ce) => {
                let args: Res<_> = ce.args.iter().map(|i| self.expr(i).map(P)).collect();
                Self::new_expr(E::Call(P(self.expr(&ce.callee)?), args?), span)
            },
            K::Switch(ref s) => {
                let arms = self.match_arms(&s.items, span)
                    .map_err(|err| err.context((format!("in switch arms {s:#?}"), span)))?;
                Self::new_expr(E::Match(P(self.expr(&s.cond)?), arms), span)
            }
            K::Return(ref r) => {
                Self::new_expr(E::Ret(if let Some(r) = r.as_ref() {
                    Some(P(self.expr(r)?))
                } else {None}), span)
            },
            K::Break => {
                Self::new_expr(E::Break(None, None), span)
            },
            K::Goto(ref name) => {
                Self::new_expr(E::Break(Some(self.spanned(span, Ident::from_str(name))), None), span)
            },
            K::Throw(ref e) => {
                let err = Self::new_expr(E::Path(None, Self::path_by_name("Err", span)), span);
                let err_call = Self::new_expr(E::Call(P(err), vec![P(self.expr(e)?)]), span);
                Self::new_expr(E::Try(P(err_call)), span)
            },
            K::Label(ref name, ref arg) => {
                let mut block = self.block(arg)?;
                block.stmts.push(Self::new_stmt_expr(span, Self::new_expr(E::Break(None, None), span)));
                Self::new_expr(E::Loop(P(block), Some(self.spanned(span, Ident::from_str(name)))), span)
            },
            K::Continue => {
                Self::new_expr(E::Continue(None), span)
            },
            K::If(ref ce) => {
                Self::new_expr(E::If(
                    P(self.expr(ce.cond.ignoring_parens())?),
                    P(self.block(ce.body.ignoring_parens())?),
                    if let Some(alt) = ce.alt.as_ref() {
                        let alt = self.expr(alt.ignoring_parens())?;
                        Some(P(match alt.node {
                            E::If(..) | E::IfLet(..) => alt, // else if
                            E::Block(b) => Self::new_expr(E::Block(b), span), // else {}
                            _ => {
                                let stmt = Self::new_stmt_expr(span, alt);
                                Self::new_block_expr(span, vec![stmt])
                            }
                        }))
                    } else {None}),
                span)
            },
            K::For(ref fo) => {
                self.for_from_expr(fo, span)?
            },
            K::ForIn(ref fo) => {
                self.for_in_from_expr(fo, span)?
            },
            K::While(ref fo) => {
                self.while_from_expr(fo, span)?
            },
            K::BinaryOperator(ref bo) => {
                let l = P(self.expr(&bo.left)?);
                let r = P(self.expr(&bo.right)?);
                let node = match bo.kind {
                    BK::Add | BK::AddPtr => E::Binary(self.spanned(span, BinOpKind::Add), l, r),
                    BK::Sub | BK::SubPtr => E::Binary(self.spanned(span, BinOpKind::Sub), l, r),
                    BK::Mul => E::Binary(self.spanned(span, BinOpKind::Mul), l, r),
                    BK::Div => E::Binary(self.spanned(span, BinOpKind::Div), l, r),
                    BK::Rem => E::Binary(self.spanned(span, BinOpKind::Rem), l, r),
                    BK::And => E::Binary(self.spanned(span, BinOpKind::And), l, r),
                    BK::Or => E::Binary(self.spanned(span, BinOpKind::Or), l, r),
                    BK::BitXor => E::Binary(self.spanned(span, BinOpKind::BitXor), l, r),
                    BK::BitAnd => E::Binary(self.spanned(span, BinOpKind::BitAnd), l, r),
                    BK::BitOr => E::Binary(self.spanned(span, BinOpKind::BitOr), l, r),
                    BK::Shl       => E::Binary(self.spanned(span, BinOpKind::Shl), l, r),
                    BK::Shr       => E::Binary(self.spanned(span, BinOpKind::Shr), l, r),
                    BK::Eq        => E::Binary(self.spanned(span, BinOpKind::Eq), l, r),
                    BK::Lt        => E::Binary(self.spanned(span, BinOpKind::Lt), l, r),
                    BK::Le        => E::Binary(self.spanned(span, BinOpKind::Le), l, r),
                    BK::Ne        => E::Binary(self.spanned(span, BinOpKind::Ne), l, r),
                    BK::Ge        => E::Binary(self.spanned(span, BinOpKind::Ge), l, r),
                    BK::Gt        => E::Binary(self.spanned(span, BinOpKind::Gt), l, r),
                    BK::MulAssign => E::AssignOp(self.spanned(span, BinOpKind::Mul), l, r),
                    BK::DivAssign => E::AssignOp(self.spanned(span, BinOpKind::Div), l, r),
                    BK::RemAssign => E::AssignOp(self.spanned(span, BinOpKind::Rem), l, r),
                    BK::AddAssign | BK::AddPtrAssign => E::AssignOp(self.spanned(span, BinOpKind::Add), l, r),
                    BK::SubAssign | BK::SubPtrAssign => E::AssignOp(self.spanned(span, BinOpKind::Sub), l, r),
                    BK::ShlAssign => E::AssignOp(self.spanned(span, BinOpKind::Shl), l, r),
                    BK::ShrAssign => E::AssignOp(self.spanned(span, BinOpKind::Shr), l, r),
                    BK::BitAndAssign => E::AssignOp(self.spanned(span, BinOpKind::BitAnd), l, r),
                    BK::BitXorAssign => E::AssignOp(self.spanned(span, BinOpKind::BitXor), l, r),
                    BK::BitOrAssign  => E::AssignOp(self.spanned(span, BinOpKind::BitOr), l, r),
                    BK::Assign => E::Assign(l, r),
                    BK::ArrayIndex => E::Index(l, r),
                    BK::PointerIndex => E::Index(l, r),
                    BK::Comma => Err(("should have been filtered out", span))?,
                    BK::HalfOpenRange => E::Range(Some(l), Some(r), RangeLimits::HalfOpen),
                    BK::ClosedRange => Err(("unimplemented", span))?,
                    BK::PointerMemberDeref => unimplemented!(),
                };
                Self::new_expr(node, span)
            },
            K::UnaryOperator(ref ce) => {
                let arg = self.expr(&ce.arg)?;
                let op = match ce.kind {
                    // Thse are implemented as filters, so should be gone by now
                    UK::PostInc | UK::PostIncPtr |
                    UK::PostDec | UK::PostDecPtr |
                    UK::PreInc | UK::PreIncPtr |
                    UK::PreDec | UK::PreDecPtr => Err(("unimplemented", span))?,
                    UK::AddrOf(is_const) => {
                        return Ok(Self::new_expr(E::AddrOf(
                            if !is_const {Mutability::Mutable} else {Mutability::Immutable},
                            P(arg)), span))
                    },
                    UK::Deref => UnOp::Deref,
                    UK::Plus => return Ok(arg),
                    UK::Minus => UnOp::Neg,
                    UK::BinNot => UnOp::Not,
                    UK::Not => UnOp::Not,
                    UK::IsNull => {
                        return Ok(self.new_method_call(span, "is_null", vec![P(arg)]))
                    },
                };

                Self::new_expr(E::Unary(op, P(arg)), span)
            },
            K::IntegerLiteral(val, ref ty) => {
                self.int_lit(val, ty.as_ref().and_then(|ty|self.lit_from_ty(ty)), self.span(ce))
            },
            K::FloatingLiteral(val, ref ty) => {
                self.float_lit(&val.to_string(), ty.as_ref().and_then(|ty|self.float_lit_from_ty(ty)), self.span(ce))
            },
            K::Paren(ref expr) => {
                match expr.kind {
                    // Avoid unnecessary parens
                    K::Block(_) | K::If(..) | K::For(..) | K::While(..) |
                    K::Paren(..) | K::Call(..) | K::SizeOf(..) |
                    K::IntegerLiteral(..) | K::FloatingLiteral(..) |
                    K::StringLiteral(..) |
                    K::BinaryOperator(expr::BinaryOperator{kind: BK::Comma, ..}) => {
                        self.expr(expr)?
                    },
                    _ => Self::new_expr(E::Paren(P(self.expr(expr)?)), span),
                }
            },
            K::Block(_) => {
                Self::new_expr(E::Block(P(self.block(ce)?)), span)
            },
            K::MemberRef(ref ce) => {
                let obj = self.expr(&ce.arg)?;
                Self::new_expr(E::Field(P(obj), self.spanned(span, Ident::from_str(&ce.name))), span)
            },
            K::OffsetOf(ref ty, ref name) => {
                eprintln!("warning: found offsetof(). Good luck with that. {:?}", ce.loc);
                let field = Self::string_lit(span, name);
                self.new_fn_call(span, "offsetof", Some(self.ty_from_ty(ty, span)?), vec![field])?
            },
            K::SizeOf(ref sz) => {
                if let Some(ref arg) = sz.arg {
                    let arg = self.expr(arg.ignoring_wrappers())?;
                    let arg = Self::new_expr(E::AddrOf(Mutability::Immutable, P(arg)), span);
                    self.new_fn_call(span, "std::mem::size_of_val", None, vec![arg])?
                } else {
                    self.new_fn_call(span, "std::mem::size_of", Some(self.ty_from_ty(&sz.ty, span)?), vec![])?
                }
            },
            K::StringLiteral(ref st) => {
                Self::string_lit(span, st)
            },
            K::CharacterLiteral(ch) => {
                Self::new_expr(E::Lit(P(Spanned{
                    node: L::Char(ch),
                    span
                })), span)
            },
            K::Invalid(ref msg) => {
                Self::new_expr(E::Paren(P(Self::new_expr(E::Lit(P(Spanned{
                    node: L::Str(Symbol::intern(msg), StrStyle::Cooked),
                    span
                })), span))), span)
            },
            K::DesignatedInit(_, ref arg) => {
                eprintln!("internal: unexpected orphaned DesignatedInit, field name lost {:?}", ce.loc);
                return self.expr(arg);
            },
            K::InitList(ref exprs) => {
                eprintln!("internal: unexpected orphaned InitList, type will be wrong {:?}", ce.loc);
                let exprs: Res<Vec<_>> = exprs.iter().map(|e|self.expr(e).map(P)).collect();
                Self::new_expr(E::Array(exprs?), span)
            },
            K::TransparentGroup(expr::TransparentGroup{ref items}) => {
                eprintln!("internal: TransparentGroup not filtered out {:?}", ce.loc);
                if items.len() == 1 {
                    return self.expr(&items[0]);
                } else {
                    Self::expr_from_stmts(self.stmts(items)?, span)
                }
            },
            _ => Err((format!("found unknown expr?? {ce:#?}"), span))?,
        })
    }

    fn lit_from_ty(&self, ty: &C3Ty) -> Option<LitIntType> {
        // FIXME: should suppor switching ILP64 / LP64 / LLP64
        // FIXME: None here may be a serious error
        Some(match ty.kind {
            TK::Short => LitIntType::Signed(IntTy::I16),
            TK::UShort => LitIntType::Unsigned(UintTy::U16),
            TK::LongLong => LitIntType::Signed(IntTy::I64),
            TK::ULongLong => LitIntType::Unsigned(UintTy::U64),
            TK::Int128 => LitIntType::Signed(IntTy::I128),
            TK::UInt128 => LitIntType::Unsigned(UintTy::U128),
            TK::SChar => LitIntType::Signed(IntTy::I8),
            TK::UChar => LitIntType::Unsigned(UintTy::U8),
            TK::Char16 => LitIntType::Unsigned(UintTy::U16),
            TK::Char32 => LitIntType::Unsigned(UintTy::U32),
            _ => return None,
        })
    }

    fn float_lit_from_ty(&self, ty: &C3Ty) -> Option<FloatTy> {
        Some(match ty.kind {
            TK::Double => FloatTy::F64,
            TK::Float => FloatTy::F32,
            _ => return None,
        })
    }

    fn no_generics(span: Span) -> Generics {
        Generics{
            lifetimes: vec![],
            ty_params: vec![],
            where_clause: WhereClause {
                id: DUMMY_NODE_ID,
                predicates: vec![],
            },
            span,
        }
    }

    fn mutability(ty: &C3Ty) -> Mutability {
        if ty.is_const {
            Mutability::Immutable
        } else {
            Mutability::Mutable
        }
    }

    fn path_by_name(name: &str, span: Span) -> Path {
        Path{
            span,
            segments: name.split("::").map(|name| PathSegment {
                identifier: Ident::from_str(name),
                parameters: None,
                span,
            }).collect(),
        }
    }

    fn type_by_name(name: &str, span: Span) -> TyKind {
        let name = match name {
            "uintptr_t" |
            "size_t" => "usize",
            "intptr_t" |
            "ssize_t" |
            "ptrdiff_t" => "isize",
            "_Bool" => "bool",
            "uint64_t" => "u64",
            "int64_t" => "i64",
            "uint32_t" => "u32",
            "int32_t" => "i32",
            "int16_t" => "i16",
            "uint16_t" => "u16",
            "int8_t" => "i8",
            "uint8_t" => "u8",
            other => other,
        };
        TyKind::Path(None, Self::path_by_name(name, span))
    }

    fn ty_from_ty(&self, ty: &C3Ty, span: Span) -> Res<Ty> {
        Ok(Ty {
            id: DUMMY_NODE_ID, span,
            node: match ty.kind {
                TK::Bool => Self::type_by_name("bool", span),
                TK::Int => Self::type_by_name("c_int", span),
                TK::UShort => Self::type_by_name("u16", span),
                TK::Short => Self::type_by_name("i16", span),
                TK::UInt => Self::type_by_name("c_uint", span),
                TK::Int128 => Self::type_by_name("i128", span),
                TK::UInt128 => Self::type_by_name("u128", span),
                TK::Long => Self::type_by_name("c_long", span),
                TK::ULong => Self::type_by_name("c_long", span),
                TK::LongLong => Self::type_by_name("c_longlong", span),
                TK::ULongLong => Self::type_by_name("c_ulonglong", span),
                TK::Float => Self::type_by_name("f32", span),
                TK::Double => Self::type_by_name("f64", span),
                TK::LongDouble => Self::type_by_name("f128", span),
                TK::SChar => Self::type_by_name("i8", span),
                TK::UChar => Self::type_by_name("u8", span),
                TK::WChar => Self::type_by_name("char", span),
                TK::Char32 => Self::type_by_name("char", span),
                TK::Char16 => Self::type_by_name("u16", span),
                TK::Void => {
                    // Could be TyKind::Tup(vec![])
                    Self::type_by_name("c_void", span)
                },
                TK::ConstantArray(sz, ref ty) => {
                    TyKind::Array(P(self.ty_from_ty(ty, span)?), P(self.int_lit(sz as u64, None, span)))
                },
                TK::VariableArray(ref sz, ref ty) => {
                    TyKind::Array(P(self.ty_from_ty(ty, span)?), P(self.expr(sz)?))
                },
                TK::IncompleteArray(ref ty) => {
                    TyKind::Slice(P(self.ty_from_ty(ty, span)?))
                },
                TK::Pointer(ref to) => {
                    TyKind::Ptr(MutTy {
                        ty: P(self.ty_from_ty(to, span)?),
                        mutbl: Self::mutability(to),
                    })
                },
                TK::Elaborated(ref name, _) |
                TK::Typedef(ref name) |
                TK::Generic(ref name) |
                TK::Struct(ref name, _) |
                TK::Union(ref name, _) => Self::type_by_name(name, span),
                TK::FunctionProto | TK::FunctionNoProto => {
                    // FIXME: these are incomplete
                    TyKind::BareFn(P(BareFnTy {
                        unsafety: Unsafety::Unsafe,
                        abi: Abi::C,
                        lifetimes: vec![],
                        decl: P(FnDecl {
                            inputs: vec![],
                            output: FunctionRetTy::Default(span),
                            variadic: true, // FIXME: made-up
                        }),
                    }))
                },
                TK::Slice(ref ty) => TyKind::Slice(P(self.ty_from_ty(ty, span)?)),
                TK::Reference(ref ty) => {
                    TyKind::Rptr(None, MutTy {
                        mutbl: Self::mutability(ty),
                        ty: P(self.ty_from_ty(ty, span)?),
                    })
                },
                TK::Auto => TyKind::Infer,
                _ => {
                    Err((format!("ERROR: Unknown kind of type {ty:#?}"), span))?
                },
            },
        })
    }

    fn pat_from_str(&self, name: &str, mutability: Mutability, span: Span) -> Pat {
        let binding = BindingMode::ByValue(mutability);
        Pat {
            id: DUMMY_NODE_ID,
            span,
            node: PatKind::Ident(binding, self.spanned(span, Ident::from_str(name)), None),
        }
    }

    fn ty_for_var(&self, ty: Option<&C3Ty>, span: Span) -> Res<Option<P<Ty>>> {
        let explicit_ty = ty.as_ref().map_or(false, |ty| ty.kind != TK::Int);
        Ok(if explicit_ty {
            if let Some(ty) = ty.as_ref() {
                Some(P(self.ty_from_ty(ty, span)?))
            } else {
                None
            }
        } else {
            None
        })
    }

    fn stmts(&self, items: &[C3Expr]) -> Res<Vec<Stmt>> {
        items.iter().map(|i| self.stmt(i)).collect()
    }

    fn stmt(&self, ce: &C3Expr) -> Res<Stmt> {
        let span = self.span(ce);
        Ok(Stmt {
            id: DUMMY_NODE_ID,
            span,
            node:  match ce.kind {
            K::VarDecl(ref var) => {
                let init = if let Some(ref e) = var.init {
                    Some(P(self.expr(e)?))
                } else {
                    None
                };
                let ty = self.ty_for_var(var.ty.as_ref(), span)
                    .map_err(|err| err.context((format!("while declaring var {}", var.name), span)))?;

                // FIXME: This is bogus. Mutability of var is not the same as const type
                let mutability = var.ty.as_ref().map_or(Mutability::Mutable, Self::mutability);
                S::Local(P(Local {
                    pat: P(self.pat_from_str(&var.name, mutability, span)),
                    // Explicit type when there is initializer ends up either redundant or conflicting
                    ty: if init.is_some() {None} else {ty},
                    init,
                    id: DUMMY_NODE_ID,
                    span,
                    attrs: ThinVec::new(),
                }))
            },
            K::TyDecl(ref decl) => {
                StmtKind::Item(P(self.item_from_ty(decl, Self::no_generics(span), span)?))
            },
            _ => { // fall back to expression
                S::Expr(P(self.expr(ce)
                        .map_err(|err| err.context(("in statement", span)))?))
            }
        }})
    }

    fn new_block_expr(span: Span, stmts: Vec<Stmt>) -> Expr {
        Self::new_expr(E::Block(P(Self::new_block(span, stmts))), span)
    }

    fn new_block(span: Span, stmts: Vec<Stmt>) -> Block {
        Block {
            id: DUMMY_NODE_ID,
            span,
            stmts,
            rules: BlockCheckMode::Default,
        }
    }

    fn block(&self, ce: &C3Expr) -> Res<Block> {
        Ok(Self::new_block(self.span(ce), match ce.kind {
            K::Block(ref bl) => {
                let mut items = self.stmts(&bl.items)?;
                // Add semicolon at end of C blocks
                if !bl.returns_value {
                    if let Some(last) = items.pop() {
                        items.push(Stmt {
                            id: DUMMY_NODE_ID,
                            span: last.span,
                            node: match last.node {
                                StmtKind::Expr(e) | StmtKind::Semi(e) => StmtKind::Semi(e),
                                other => other,
                            },
                        });
                    }
                }
                items
            },
            K::TransparentGroup(ref bl) => {
                self.stmts(&bl.items)?
            },
            _ => vec![
                self.stmt(ce)?
            ],
        }))
    }

    fn for_in_from_expr(&self, ce: &expr::ForIn, span: Span) -> Res<Expr> {
        let var_name = match ce.pattern.kind {
            K::DeclRef(ref name) => name,
            _ => Err(("unsupported", span))?,
        };
        Ok(Self::new_expr(
            E::ForLoop(
                P(self.pat_from_str(var_name, Mutability::Mutable, span)),
                P(self.expr(&ce.iter)?),
                P(self.block(&ce.body)?),
                None,
            ),
            span,
        ))
    }

    // Move that to morph phase
    fn for_from_expr(&self, ce: &expr::For, span: Span) -> Res<Expr> {
        let mut block = self.block(&ce.body)?;
        if let Some(ref inc) = ce.inc {
            block.stmts.push(Self::new_stmt_expr(span, self.expr(inc)?));
        }

        let body = Self::new_expr(
            if let Some(ref cond) = ce.cond {
                E::While(P(self.expr(cond)?), P(block), None)
            } else {
                E::Loop(P(block), None)
            },
            span,
        );

        assert!(ce.init.is_none()); // should be filtered out earlier
        Ok(body)
    }

    fn while_from_expr(&self, ce: &expr::While, span: Span) -> Res<Expr> {
        if ce.is_do {
            Err(("this should have been transformed", span))?;
        }
        Ok(Self::new_expr(E::While(
            P(self.expr(&ce.cond)?),
            P(self.block(&ce.body)?),
            None,
        ), span))
    }

    fn fn_decl(&self, ce: &expr::FunctionDecl, prototype: bool, span: Span) -> Res<FnDecl> {
        let inputs = ce.args.iter().map(|arg|{
            let span = self.span_loc(&arg.loc);
            Ok(Arg {
                id: DUMMY_NODE_ID,
                ty: P(self.ty_from_ty(&arg.ty, span)?),
                pat: P(Pat {
                    id: DUMMY_NODE_ID,
                    span,
                    node: PatKind::Ident(
                        // Prototypes in Rust don't allow patterns (mut x: is a pattern)
                        BindingMode::ByValue(if prototype {Mutability::Immutable} else {Self::mutability(&arg.ty)}),
                        self.spanned(span, Ident::from_str(&arg.name)),
                        None,
                    )
                }),
            })
        }).collect::<Res<_>>()?;
        Ok(FnDecl{
            inputs,
            output: if ce.ty.kind == TK::Void {
                FunctionRetTy::Default(span)
            } else {
                FunctionRetTy::Ty(P(self.ty_from_ty(&ce.ty, span)?))
            },
            variadic: ce.variadic && !ce.args.is_empty(),
        })
    }

    fn fn_from_expr(&self, ce: &expr::FunctionDecl, body: &C3Expr, generics: Generics, span: Span) -> Res<Item> {
        let body = self.block(body)
            .map_err(|e|e.context((format!("in fn {}", ce.name), span)))?;
        let decl = self.fn_decl(ce, false, span)?;
        Ok(Item {
            id: DUMMY_NODE_ID,
            ident: Ident::from_str(&ce.name),
            span,
            attrs: if ce.storage != Storage::Static && ce.abi != ty::Abi::Rust {vec![Self::no_mangle(span)]} else {vec![]},
            vis: if ce.storage != Storage::Static {Visibility::Public} else {Visibility::Inherited},
            node: I::Fn(
                P(decl),
                Unsafety::Unsafe,
                self.spanned(span, Constness::NotConst),
                match ce.abi {
                    ty::Abi::C => Abi::C,
                    ty::Abi::Rust => Abi::Rust,
                    ty::Abi::Stdcall => Abi::Stdcall,
                    ty::Abi::Fastcall => Abi::Fastcall,
                    ty::Abi::Vectorcall => Abi::Vectorcall,
                    ty::Abi::Thiscall => Abi::Thiscall,
                    ty::Abi::Aapcs => Abi::Aapcs,
                    ty::Abi::Win64 => Abi::Win64,
                    ty::Abi::SysV64 => Abi::SysV64,
                },
                generics,
                P(body),
            ),
        })
    }

    fn no_mangle(span: Span) -> Attribute {
        Attribute {
            id: AttrId(0),
            style: AttrStyle::Outer,
            path: Self::path_by_name("no_mangle", span),
            tokens: TokenStream::empty(),
            is_sugared_doc: false,
            span,
        }
    }

    fn items(&self, items: &[C3Expr]) -> Res<Vec<P<Item>>> {
        items
            .iter()
            .map(|i| self.item_from_expr(i).map(P))
            .collect()
    }

    fn new_item(name: &str, span: Span, node: ItemKind) -> Item {
        Item {
            id: DUMMY_NODE_ID,
            ident: Ident::from_str(name),
            attrs: vec![],
            span,
            vis: Visibility::Public,
            node,
        }
    }

    pub fn crate_from_expr(&self, ce: &C3Expr) -> Res<Crate> {
        let span = self.span(ce);
        Ok(Crate {
            attrs: vec![],
            span,
            module: Mod {
                inner: span,
                items: match ce.kind {
                    K::TranslationUnit(ref tu) => self.items(&tu.items)?,
                    _ => vec![P(self.item_from_expr(ce)?)],
                },
            },
        })
    }

    pub fn foreign_item_from_expr(&self, ce: &C3Expr) -> Res<ForeignItem> {
        let span = self.span(ce);
        let generics = Self::no_generics(span);
        let ident;
        let node = match ce.kind {
            K::FunctionProtoDecl(ref fun) => {
                ident = Ident::from_str(&fun.name);
                ForeignItemKind::Fn(P(self.fn_decl(fun, true, span)?), generics)
            },
            _ => return Err((format!("expr can't be an item in extern C: {ce:?}"), span))?,
        };
        Ok(ForeignItem {
            ident,
            attrs: vec![],
            node,
            id: DUMMY_NODE_ID,
            span,
            vis: Visibility::Public,
        })
    }

    pub fn item_from_expr(&self, ce: &C3Expr) -> Res<Item> {
        let span = self.span(ce);
        let generics = Self::no_generics(span);
        Ok(match ce.kind {
            K::TranslationUnit(ref tu) => {
                let path = FSPath::new(&tu.name);
                let name = path.file_stem().unwrap_or(path.as_os_str()).to_string_lossy();
                let mut name:String = name.chars().map(|c| match c {
                    x if x.is_alphanumeric() => x,
                    _ => '_',
                }).collect();
                if !name.chars().next().map_or(false, |c|c.is_alphabetic()) {
                    name = format!("_{name}");
                }
                Self::new_item(&name, span, I::Mod(Mod{
                    inner: span,
                    items: self.items(&tu.items)?,
                }))
            },
            K::TyDecl(ref decl) => {
                self.item_from_ty(decl, generics, span)?
            },
            K::FunctionDecl(ref f, ref body) => {
                self.fn_from_expr(f, body, generics, span)?
            },
            K::VarDecl(ref var) => {
                let init = var.init.as_ref().map(|e| self.expr(e));
                let init = init.unwrap_or_else(||Ok(self.make_incomplete_expr_tuple(vec![], span)))?;
                let var_ty = var.ty.as_ref().ok_or(("global var without type?",span))?;
                let ty = self.ty_from_ty(var_ty, span)
                    .map_err(|err| err.context((format!("while declaring var {}", var.name), span)))?;
                Self::new_item(&var.name, span, I::Static(
                    P(ty),
                    Self::mutability(var_ty),
                    P(init),
                ))
            },
            K::ExternC(ref items) => {
                Self::new_item("", span, I::ForeignMod(ForeignMod {
                    abi: Abi::C,
                    items: items.iter().map(|i| self.foreign_item_from_expr(i)).collect::<Res<_>>()?,
                }))
            },
            K::ClassDecl(ref cls) => {
                Self::new_item(&cls.name, span, I::Impl(
                    Unsafety::Unsafe,
                    ImplPolarity::Positive,
                    Defaultness::Final,
                    generics,
                    None,
                    P(Ty {id: DUMMY_NODE_ID, span, node: Self::type_by_name(&cls.name, span)}),
                    vec![/*FIXME*/]))
            },
            K::Namespace(ref name, ref items) => {
                Self::new_item(name, span, I::Mod(Mod {
                    inner: span,
                    items: self.items(items)?,
                }))
            },
            K::MacroRef(ref name) => {
                Self::new_item(name, span, I::Mac(self.spanned(span, Mac_ {
                    path: Self::path_by_name(name, span),
                    tts: TokenStream::empty().into(),
                })))
            },
            _ => Err((format!("unknown top item: {ce:#?}"), span))?,
        })
    }

    fn spanned<T>(&self, span: Span, node: T) -> Spanned<T> {
        Spanned { node, span }
    }

    fn item_from_ty(&self, decl: &expr::TyDecl, generics: Generics, span: Span) -> Res<Item> {
        Ok(Item {
            id: DUMMY_NODE_ID,
            ident: if decl.name.contains(' ') {
                Ident::from_str(&decl.name.replace(' ',"_"))
            } else {
                Ident::from_str(&decl.name)
            },
            attrs: vec![], span,
            vis: Visibility::Public,
            node: match decl.ty.kind {
                TK::Enum(_, ref items) => {
                    let variants: Res<Vec<_>> = items.iter().map(|ce|{
                        Ok(self.spanned(span, Variant_ {
                            name: Ident::from_str(&ce.name),
                            attrs: vec![],
                            data: VariantData::Unit(NodeId::new(0)),
                            disr_expr: if let Some(ref init) = ce.value {
                                Some(P(self.expr(init)?))
                            } else {
                                None
                            },
                        }))
                    }).collect();
                    I::Enum(EnumDef{ variants: variants?}, generics)
                },
                TK::Struct(_, ref items) => {
                    I::Struct(VariantData::Struct(self.struct_fields(items)?, NodeId::new(0)), generics)
                },
                TK::Union(_, ref items) => {
                    I::Union(VariantData::Struct(self.struct_fields(items)?, NodeId::new(0)), generics)
                },
                _ => {
                    let ty = self.ty_from_ty(&decl.ty, span)
                        .map_err(|err| err.context((format!("in fallback top-level {}", decl.name), span)))?;
                    I::Ty(P(ty), generics)
                },
            },
        })
    }

    fn struct_fields(&self, items: &[ty::Field]) -> Res<Vec<StructField>> {
         items.iter().map(|field|{
            let span = self.span_loc(&field.loc);
            let ty = self.ty_from_ty(&field.ty, span)
                .map_err(|err| err.context(format!("in struct field {}", field.name)))?;
            Ok(StructField {
                span,
                ident: Some(Ident::from_str(&field.name)),
                vis: Visibility::Public,
                id: NodeId::new(0),
                ty: P(ty),
                attrs: vec![],
            })
        }).collect()
    }


    fn expr_from_stmts(mut stms: Vec<Stmt>, span: Span) -> Expr {
        if stms.len() == 1 {
            let is_expr = match &stms[0].node {
                &StmtKind::Expr(_) => true,
                _ => false,
            };
            if is_expr {
                return match stms.remove(0).node {
                    StmtKind::Expr(e) => e.unwrap(),
                    _ => unreachable!(),
                };
            }
        }
        Self::new_expr(E::Block(P(Self::new_block(span, stms))), span)
    }

    fn match_arms(&self, exprs: &[C3Expr], span: Span) -> Res<Vec<Arm>> {
        exprs.iter().map(|e| {
            Ok(match e.kind {
                K::Case(ref ca) => {
                    let body = self.stmts(&ca.items)?;
                    let pats = if !ca.conds.is_empty() {
                        ca.conds.iter()
                            .map(|e| e.ignoring_parens())
                            .map(|e| Ok(P(Pat {
                                id: DUMMY_NODE_ID,
                                node: PatKind::Lit(P(self.expr(e)?)),
                                span,
                            })))
                            .collect::<Res<_>>()?
                    } else {
                        vec![P(Pat {
                            id: DUMMY_NODE_ID,
                            node: PatKind::Wild,
                            span,
                        })]
                    };
                    Arm {
                        guard: None,
                        pats,
                        attrs: vec![],
                        body: P(Self::expr_from_stmts(body, span)),
                   }
                },
                _ => Err((format!("expected only Case children of Switch, got {e:#?}"), span))?,
            })
        }).collect()
    }
}
