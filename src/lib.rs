//! C to Rust AST->AST translator
//!
//! See `tx` mod for the translation source code.

#![allow(unused_imports)]
#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(non_upper_case_globals)]
#![allow(unreachable_code)]
#![allow(non_snake_case)]
#![deny(unreachable_patterns)]


use std::fs;
use std::path::Path;
use std::env;
use std::process::Command;
use std::rc::Rc;

mod tx;
mod txerror;
mod morph;
mod exprinfo;
mod builder;
use c3::expr::Expr;

use crate::txerror::*;

use syntex_syntax::print::pprust;
use syntex_syntax::print::pp;
use syntex_syntax::ast;
use syntex_syntax::codemap::*;
use syntex_syntax::parse::lexer::comments;

pub struct Citrus {
    default_compiler_flags: Vec<String>,
    codemap: CodeMap,
    options: Options,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum API {
    /// Use only C-compatible types and export functions
    C,
    /// Export public functions as C-compatible. Private functions use Rust types.
    ExternC,
    /// Allow use of Rust types anywhere. Don't export functions to C.
    Rust,
}

impl Default for API {
    fn default() -> Self {
        Self::ExternC
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Options {
    pub api: API,
}

impl Citrus {
    pub fn new(options: Options) -> Res<Self> {
        let mut default_compiler_flags = system_compiler_flags()?;
        default_compiler_flags.extend(clang_default_flags()?);

        // intrinsics are not supported yet
        #[cfg(any(target_arch = "x86_64", target_arch = "x86"))]
        default_compiler_flags.push("-mno-sse".into());

        let codemap = CodeMap::new(FilePathMapping::empty());

        Ok(Self {
            default_compiler_flags,
            codemap,
            options,
        })
    }
}

impl Citrus {
    /// Convert C source code to Rust source code
    ///
    /// Compiler flags are flags accepted by Clang, e.g. `-Idir/` and `-Wall`
    pub fn transpile_source(&self, source: &str, original_file_path: &Path, compiler_flags: &[String]) -> Res<String> {
        self.transpile_source_impl(source, original_file_path, compiler_flags)
            .map_err(|e|self.resolve_err(e))
    }

    fn transpile_source_impl(&self, source: &str, original_file_path: &Path, compiler_flags: &[String]) -> Res<String> {
        let filemap = self.codemap.new_filemap_and_lines(original_file_path.to_str().ok_or("bad filename")?, source);
        let tree = c3::C3::parse_source(source, original_file_path, &self.flags_for_file(compiler_flags, original_file_path))?;
        self.transpile_c3(tree, filemap)
    }

    /// Convert C file to Rust source code
    pub fn transpile_file(&self, file_path: &Path, compiler_flags: &[String]) -> Res<String> {
        self.transpile_file_impl(file_path, compiler_flags)
            .map_err(|e|self.resolve_err(e))
    }

    fn transpile_file_impl(&self, file_path: &Path, compiler_flags: &[String]) -> Res<String> {
        let filemap = self.codemap.load_file(file_path)?;
        let tree = c3::C3::parse_file(file_path, &self.flags_for_file(compiler_flags, file_path))?;
        self.transpile_c3(tree, filemap)
    }

    fn transpile_c3(&self, mut tree: c3::C3, filemap: Rc<FileMap>) -> Res<String> {
        let root = tree.ast()?;
        let com = tree.comments()?;
        let com = com.into_iter().map(|c| {
            let is_long = c.syntax.len() >= 75 || c.loc.start_col as usize + c.syntax.len() > 80;
            let lines: Vec<_> = c.syntax.split('\n').map(|s|s.to_owned()).collect();
            comments::Comment {
                style: if !c.after_code && (is_long || lines.len() > 1 || c.loc.start_col <= 4) {
                    comments::CommentStyle::Isolated
                } else {
                    comments::CommentStyle::Trailing
                },
                lines,
                pos: filemap.start_pos + BytePos(c.loc.byte_pos),
            }
        }).collect();
        self.pretty_print(self.transpile_ast(root, filemap)?, com)
    }

    fn resolve_err(&self, mut err: Error) -> Error {
        err.resolve(&self.codemap, true);
        err
    }

    /// Takes AST parsed by C3
    ///
    /// Cleans it up, makes it more Rust-compatible (e.g. ++x => x+=1),
    /// and converts to Rust AST
    pub fn transpile_ast(&self, mut root: Expr, filemap: Rc<FileMap>) -> Res<ast::Crate> {
        morph::filter(&mut root, &self.options);
        let tx = tx::Tx::new(&root, filemap)?;
        tx.translate_crate()
    }

    fn pretty_print(&self, krate: ast::Crate, comments: Vec<comments::Comment>) -> Res<String> {
        let mut wr = Vec::new();
        {
            let mut printer = pprust::State::new(&self.codemap, Box::new(&mut wr), &pprust::NoAnn, Some(comments), None);
            printer.print_mod(&krate.module, &krate.attrs)?;
            pp::eof(&mut printer.s)?;
        }
        Ok(String::from_utf8(wr).map_err(|_|"bad encoding of output")?)
    }

    /// C files are loaded outside of their usual build system,
    /// and libclang doesn't have clang's usual defaults,
    /// so a lot of fudging is needed to set up header include paths.
    fn flags_for_file(&self, user_flags: &[String], file_path: &Path) -> Vec<String> {
        let mut compiler_flags = self.default_compiler_flags.clone();
        compiler_flags.extend_from_slice(user_flags);

        match file_path.extension().and_then(|s|s.to_str()) {
            Some("c") => compiler_flags.push("-std=c11".into()),
            Some("m") => compiler_flags.push("-xobjective-c".into()),
            Some("hpp" | "cpp") => {
                compiler_flags.push("-xc++".into());
                compiler_flags.push("-stdlib=libc++".into());
            },
            _ => (),
        }

        let canon = fs::canonicalize(file_path);
        let parent = if let Ok(ref p) = canon {
            p.parent()
        } else {
            file_path.parent()
        };
        compiler_flags.push(format!("-I{}", parent.unwrap_or(Path::new(".")).display()));
        let parent = parent.and_then(|p|p.parent());
        compiler_flags.push(format!("-I{}", parent.unwrap_or(Path::new("..")).display()));
        compiler_flags
    }
}

fn command_output(cmd_args: &[&str]) -> Res<String> {
    let mut cmd = Command::new(cmd_args[0]);
    for arg in &cmd_args[1..] {
        cmd.arg(arg);
    }
    let res = cmd.output()?;
    if !res.status.success() {
        Err(format!("can't run {cmd_args:?}"))?;
    }
    Ok(String::from_utf8(res.stdout).map_err(|_|"garbage command output")?)
}

fn clang_default_flags() -> Res<Vec<String>> {
    let mut flags = vec![];

    for p in c3::C3::include_paths() {
        flags.push(format!("-I{}", p.display()));
    }

    // libclang lacks any include paths by default,
    // and compiler's own dir is required for compiler-specific headers,
    // so scrape them from clang
    let clang_paths = command_output(&["clang", "-print-search-dirs"])
        .map_err(|e| e.context("unable to run clang to get system include paths"))?;
    let clang_paths = clang_paths.split('\n')
        .filter(|l| l.starts_with("libraries: ="))
        .map(|s| &s["libraries: =".len()..]);
    for paths in clang_paths {
        for path in env::split_paths(paths) {
            let include_path = path.join("include");
            let parent_include_path = path.join("../include");
            let path = if include_path.exists() {
                include_path
            } else if parent_include_path.exists() {
                parent_include_path
            } else {
                path
            };
            flags.push(format!("-I{}", path.display()));
        }
    }
    Ok(flags)
}

#[cfg(target_os = "macos")]
fn system_compiler_flags() -> Res<Vec<String>> {
    // The SDK contains non-compiler-specific system headers
    let xcode_path = command_output(&["xcrun", "--show-sdk-path"])
        .map_err(|e| e.context("unable to get SDK path from xcrun"))?;

    Ok(vec![
        format!("-I{}/usr/include", xcode_path.trim()),
        format!("-I{}/usr/include/c++/4.2.1", xcode_path.trim()),
        format!("-F{}/System/Library/Frameworks", xcode_path.trim()),
        "-I/usr/include".to_owned(),
        "-I/usr/local/include".to_owned(),

        // macOS header errors
        "-Wno-nullability-completeness".to_owned(),
        "-Wno-expansion-to-defined".to_owned(),
    ])
}

#[cfg(not(target_os = "macos"))]
fn system_compiler_flags() -> Res<Vec<String>> {
    Ok(vec![
        "-I/usr/include".to_owned(),
        "-I/usr/local/include".to_owned(),
    ])
}
