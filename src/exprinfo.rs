use c3::expr::*;
use c3::ty::*;
use c3::expr::Kind as K;
use c3::ty::TyKind as TK;

pub trait TyInfo {
    fn target_type(&self) -> Option<&Ty>;
}

impl TyInfo for Ty {
    fn target_type(&self) -> Option<&Ty> {
        match self.kind {
            TK::ConstantArray(_, ref ty) |
            TK::VariableArray(_, ref ty) |
            TK::IncompleteArray(ref ty) |
            TK::Pointer(ref ty) => Some(ty),
            _ => None,
        }
    }
}


pub trait SwitchInfo {
    fn has_default(&self) -> bool;
}

impl SwitchInfo for Switch {
    fn has_default(&self) -> bool {
        self.items.iter().any(|e| match e.kind {
            K::Case(ref c) => c.conds.is_empty(),
            _ => panic!("non-case in switch"),
        })
    }
}

pub trait ExprInfo {
    fn is_unconditionally_returning(&self) -> bool;
    fn ignoring_wrappers(&self) -> &Expr;
    fn ignoring_wrappers_mut(&mut self) -> &mut Expr;
    fn ignoring_parens(&self) -> &Expr;
    fn var_name(&self) -> Option<&str>;
}

impl ExprInfo for Expr {
    fn var_name(&self) -> Option<&str> {
        match self.ignoring_wrappers().kind {
            K::DeclRef(ref name) => Some(name),
            K::UnaryOperator(ref un) => un.arg.var_name(), // So foo++ still sees the name
            _ => None,
        }
    }

    fn ignoring_wrappers(&self) -> &Expr {
        match self.kind {
            K::Cast(Cast{ref arg, ..}) => arg.ignoring_wrappers(),
            K::Label(_, ref arg) => arg.ignoring_wrappers(),
            K::Paren(ref arg) => arg.ignoring_wrappers(),
            K::TransparentGroup(TransparentGroup{ref items}) if items.len() == 1 => items[0].ignoring_wrappers(),
            _ => self,
        }
    }

    fn ignoring_wrappers_mut(&mut self) -> &mut Expr {
        match self.kind {
            K::Cast(Cast{ref mut arg, ..}) => arg.ignoring_wrappers_mut(),
            K::Label(_, ref mut arg) => arg.ignoring_wrappers_mut(),
            K::Paren(ref mut arg) => arg.ignoring_wrappers_mut(),
            _ => self,
        }
    }

    fn ignoring_parens(&self) -> &Expr {
        match self.kind {
            K::Paren(ref arg) => arg.ignoring_parens(),
            _ => self,
        }
    }

    fn is_unconditionally_returning(&self) -> bool {
        match self.kind {
            K::Switch(ref c) => c.has_default() && c.items.iter().all(Self::is_unconditionally_returning),
            K::Case(ref c) => c.items.iter().any(Self::is_unconditionally_returning),
            K::Block(ref c) => c.items.iter().any(Self::is_unconditionally_returning),
            K::TransparentGroup(ref c) => c.items.iter().any(Self::is_unconditionally_returning),
            K::TranslationUnit(ref c) => c.items.iter().any(Self::is_unconditionally_returning),

            K::Paren(ref c) => c.is_unconditionally_returning(),
            K::Label(_, ref c) => c.is_unconditionally_returning(),
            K::Cast(ref c) => c.arg.is_unconditionally_returning(),
            K::MemberRef(ref c) => c.arg.is_unconditionally_returning(),
            K::UnaryOperator(ref c) => c.arg.is_unconditionally_returning(),
            K::BinaryOperator(ref c) => {
                c.left.is_unconditionally_returning() || c.right.is_unconditionally_returning()
            },
            K::If(ref c) => if let Some(ref alt) = c.alt {
                alt.is_unconditionally_returning() && c.body.is_unconditionally_returning()
            } else {false},
            K::Return(..) => true,

            // Ignoring cases where it theoretically could, but not likely
            K::For(..) |
            K::ForIn(..) |
            K::While(..) => false,
            K::DeclRef(..) |
            K::MacroRef(..) |
            K::SizeOf(..) |
            K::OffsetOf(..) |
            K::IntegerLiteral(..) |
            K::StringLiteral(..) |
            K::FloatingLiteral(..) |
            K::CharacterLiteral(..) |
            K::DesignatedInit(..) |
            K::TyDecl(..) |
            K::RecordDecl(..) |
            K::FunctionDecl(..) |
            K::FunctionProtoDecl(..) |
            K::VarDecl(..) |
            K::InitList(..) |
            K::CompoundLiteral(..) => false,
            K::Call(..) |
            K::ExternC(..) |
            K::Invalid(..) |
            K::Goto(..) |
            K::Break |
            K::Continue => false,
            K::Throw(..) |
            K::Try(..) |
            K::Namespace(..) |
            K::ClassDecl(..) |
            K::CXXConstructor(..) |
            K::CXXDestructor(..) |
            K::CXXMethod(..) => panic!("Unsupported {self:?}"),
        }
    }
}

pub trait ExprPriority {
    fn priority(&self) -> Option<u8>;
}

impl ExprPriority for Expr {
    fn priority(&self) -> Option<u8> {
        self.kind.priority()
    }
}

impl ExprPriority for UnaryOperator {
    fn priority(&self) -> Option<u8> {
        use self::UnOpKind::*;
        Some(match self.kind {
            PostInc |
            PostDec |
            PostIncPtr |
            PostDecPtr => 1,
            AddrOf(_) => 3, // I guess?
            _ => 2, // Unary operators have the same precedence level and are stronger than any of the binary operators
        })
    }
}

impl ExprPriority for BinaryOperator {
    fn priority(&self) -> Option<u8> {
        use self::BinOpKind::*;
        Some(match self.kind {
            ArrayIndex | PointerIndex => 3,
            PointerMemberDeref => 4,
            Mul | Div | Rem => 5,
            Add | Sub | AddPtr | SubPtr => 6,
            Shl | Shr => 7,
            BitAnd => 8,
            BitXor => 9,
            BitOr => 10,
            Eq | Ne | Lt | Gt | Le | Ge => 11,
            And => 12,
            Or => 13,
            ClosedRange | HalfOpenRange => 14,
            Assign | MulAssign | DivAssign | RemAssign | AddAssign | SubAssign |
            AddPtrAssign | SubPtrAssign | ShlAssign | ShrAssign |
            BitAndAssign | BitXorAssign | BitOrAssign => 15,
            Comma => 16,
        })
    }
}

impl ExprPriority for Call {
    fn priority(&self) -> Option<u8> {
        Some(1)
    }
}

impl ExprPriority for Cast {
    fn priority(&self) -> Option<u8> {
        if self.explicit {Some(4)} else {self.arg.priority()}
    }
}

impl ExprPriority for MemberRef {
    fn priority(&self) -> Option<u8> {
        Some(1)
    }
}

impl ExprPriority for Kind {
    fn priority(&self) -> Option<u8> {
        match *self {
            Self::Paren(_) | Self::Block(_) => Some(0),
            Self::MemberRef(ref c) => c.priority(),

            Self::Call(ref c) => c.priority(),
            Self::Cast(ref c) => c.priority(),
            Self::UnaryOperator(ref un) => un.priority(),
            Self::BinaryOperator(ref bo) => bo.priority(),
            _ => None,
        }
    }
}
